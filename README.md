# OSIS Green New Testament

OSIS XML files of the New Testament based on Tischendorf's 8th Greek New Testament, using Strong's numbering and Robinson's morph codes.